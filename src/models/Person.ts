class Person {
  constructor(public id: string, public firstName: string, public lastName: string, public age: number) {
  }
}

export default Person
