import express from 'express'
import todoRouter from './routers/person.router'
import { json } from 'body-parser'

const server = express()
const PORT = 3333
server.use(json())

server.use('/persons', todoRouter)

server.listen(PORT, () => console.log(`Server is started at port ${PORT}`))
